<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Cause;
use App\Classes\PushNotification;
use App\Classes\ResponseClass;
use App\Classes\StudentLevel;
use App\User;
use App\UserActivity;
use App\YcspActiveUser;
use App\YcspActivityReview;
use App\YcspCommunityLink;
use App\YcspDevice;
use App\YcspGamificationPoint;
use App\YcspMcqs;
use App\YcspMcqsQuestion;
use App\YcspPost;
use App\YcspPostComment;
use App\YcspStudentReward;
use App\YcspUsersAnswer;
use Carbon\Carbon;
use Config;
use File;
use Input;
use Lang;
use Request;
use URL;

class CauseController extends Controller
{
    /*
     * Request URL: http://domain/api/v1/causeData/id
     * Method: GET
     * Params: causeID
     * Return: success or error$this
     * Comment: Used for cause data
     */
    public function index($id, Request $request)
    {
        $authUserInfo = Request::get('userInfo');
        $finalCauseData = array();
        if (isset($id) && is_numeric($id)) {
            $causeID = $id;
            $causeData = Cause::where('id', $causeID)
                ->where('status', '<>', 0)
                ->first();
            if (!$causeData) {
                return ResponseClass::prepareResponse(
                    '',
                    'error',
                    'COMMON_ERROR_MESSAGE'
                );
            }
            $sectionData = $causeData->sections()->orderBy('id', 'DESC')->get();
            $causeArray = array(
                'id' => $causeData->id,
                'heading' => $causeData->title_ar,
                'discussion_board' => $causeData->discussion_board
            );
            $i = 1;
            foreach ($sectionData as $section) {
                // to skip campaign activity
                if ($section->id == '5') {
                    continue;
                }
                $causeArray['section'.$i] = array();
                $causeArray['section'.$i]['id'] = $section->id;
                $causeArray['section'.$i]['heading'] = $section->title_ar;
                $causeArray['section'.$i]['cause_id'] = $causeData->id;
                $j = 0;
                $sectionActivities = $section->sectionActivities()->orderBy('id')->get();
                if (!empty($sectionActivities)) {
                    foreach ($sectionActivities as $activity) {
                        if ($activity->activityType->id == 2) {
                            $videoData = json_decode($activity->link_data);
                            foreach ($videoData as $key => $data) {
                                if ((preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data->button_value, $match))) {
                                    $embedURL =  'https://www.youtube.com/embed/'.$match[1].'';
                                    $videoData[$key]->button_value = $embedURL;
                                }
                            }
                            $activity->link_data = $videoData;
                        }
                        if ($activity->activityType->id == 1) {
                            if ((preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $activity->description, $match))) {
                                $embedURL =  'https://www.youtube.com/embed/'.$match[1].'';
                                $activity->description = $match[1];
                            }
                        }
                        $buttons = array();
                        if ($activity->link_data) {
                            if (is_array($activity->link_data)) {
                                $activity->link_data = json_encode($activity->link_data);
                            }
                            $buttonData = json_decode($activity->link_data);
                            foreach ($buttonData as $key => $button) {
                                $buttons[$key]['button_text'] = $button->button_text;
                                if (isset($button->button_value)) {
                                    $buttons[$key]['button_value'] = $button->button_value;
                                }
                            }
                        }
                        $studentActivity = $activity->userActivity()->where('user_id', $authUserInfo->id)->where('cause_id', $causeID)->first();
                        if (count($studentActivity)) {
                            $flag = 1;
                        } else {
                            $flag = 0;
                        }

                        // Check is any other user rather then student has commented on this activity
                        if ($studentActivity) {
                            $reviewExist = YcspActivityReview::where('user_activity_id', $studentActivity->id)->first();
                            if ($reviewExist) {
                                $reviewExist = true;
                            } else {
                                $reviewExist = false;
                            }
                        } else {
                            $reviewExist = false;
                        }
                        $activityArray = array(
                            "id" => $activity->id,
                            "name" => $activity->name_ar,
                            "type" => $activity->type,
                            "description" => $activity->description,
                            "link_type" => $activity->link_type,
                            "link_data" => $activity->link_data,
                            "buttons" => $buttons,
                            "activity_type" => $activity->activityType->id,
                            "flag" => $flag,
                            "image" => $activity->image,
                            'reviewExist' => $reviewExist,
                            'activity_type_id' => $activity->activity_type_id
                        );

                        $causeArray['section'.$i]['activity'][$j]= $activityArray;
                        $j++;
                    }
                } else {
                    $causeArray['section'.$i]['activity'] = array();
                }
                $i++;
            }
            //survey link
            $survey = YcspCommunityLink::where('type', 'student_survey')->first();
            if ($survey) {
                $causeArray['survey'] = $survey;
            }
            $userID = $authUserInfo->id;
            $causeArray['userProgress'] = $this->userProgress($causeID, $request);
            $userActivity = UserActivity::where('user_id', $userID)
                ->where('cause_id', $causeData->id)
                ->get();
            $causeArray['userActivity'] = $userActivity;
            if ($authUserInfo->image) {
                if (!file_exists(public_path().'/uploads/profile/thumb/'.$authUserInfo->image)) {
                    $authUserInfo->image = '';
                }
            }
            $causeArray['authUserInfo'] = $authUserInfo;
            $mcqActivity = Activity::where('activity_type_id', 9)->first();
            $causeArray['mcqActivity'] = $mcqActivity;
            return ResponseClass::prepareResponse(
                $causeArray,
                'success',
                ''
            );
            return ResponseClass::prepareResponse(
                '',
                'error',
                'COMMON_ERROR_MESSAGE'
            );
        }
        return ResponseClass::prepareResponse(
            '',
            'error',
            Lang::get('message.noParameters')
        );
    }
    
    /*
     * Request URL: http://domain/api/v1/CauseQuestions
     * Method: Post
     * Params: causeID, offset
     * Return: success or error
     * Comment: Used to get all questions
     */
    public function CauseQuestions(Request $request)
    {
        $causeID = Input::get('causeID');
        $authUserInfo = Request::get('userInfo');
        $userID = $authUserInfo->id;
        $countQuestion = 0;
        if (isset($causeID)) {
            $causeData = Cause::find($causeID);
            $mcqData = $causeData->mcqs()->orderBy('id', 'ASC')->get();
            $questionArray = array();
            if ($mcqData) {
                $mcqIDs = array();
                foreach ($mcqData as $key => $mcq) {
                    array_push($mcqIDs, $mcq->id);
                }
                $totalQuestions = YcspMcqsQuestion::whereIn('mcq_id', $mcqIDs)->count();
                foreach ($mcqData as $key => $mcq) {
                    $mcqTitle = $mcq->title;
                    $count = $mcq->userAnswers()->where('user_id', $userID)->count();
                    $countQuestion = $countQuestion + $count;
                    $mcqQuestions = $mcq->mcqsQuestions()->count();
                    if ($count < $mcqQuestions) {
                        if ($count == 0) {
                            $offset = 0;
                            $questionNumber = 1;
                        } else {
                            $offset = $count;
                            $questionNumber = $count + 1;
                        }
                        if ($count < $totalQuestions) {
                            $questions = $mcq->mcqsQuestions()
                                ->orderBy('id', 'ASC')
                                ->offset($offset)
                                ->limit(1)
                                ->first();
                            $answers = $questions->answer()->orderBy('id', 'ASC')->get();
                            $mcqTitle = YcspMcqs::find($mcq->id);
                            break;
                        }
                    }
                }
                if (!isset($questions)) {
                    $questions = array();
                }
                if (!isset($answers)) {
                    $answers = array();
                }
            } else {
                $mcqData = array();
                $questions = array();
                $answers = array();
                $totalQuestion = 0;
                $questionNumber = 0;
                $submittedAnswer = '';
                $mcqTitle = '';
            }
            if ($countQuestion == 0) {
                $countQuestion = false;
            }
            return ResponseClass::prepareResponse(
                array(
                    'mcqData' => $mcqData,
                    'question' => $questions,
                    'answers' => $answers,
                    'totalQuestion' => $totalQuestions,
                    'questionNumber' => $countQuestion + 1,
                    'submittedAnswer' => $countQuestion,
                    'mcqTitle' => $mcqTitle
                ),
                'success',
                ''
            );
        }
        return ResponseClass::prepareResponse(
            '',
            'error',
            Lang::get('message.noParameters')
        );
    }

    /*
     * Request URL: http://domain/api/v1/Posts
     * Method: Post
     * Params: pageNo, perPage, causeID
     * Return: success or error
     * Comment: Used to get all discussion board questions
     */
    public function Posts()
    {
        $discussionBoard = array();
        $causeID = Input::get('causeID') ? Input::get('causeID') : 1;
        $pageNo = Input::get('pageNo') ? Input::get('pageNo') : 1;
        $recPerPage = Input::get('perPage') ? Input::get('perPage') : 10;
        $postType = Input::get('postType') ? Input::get('postType') : 'all';
        $offset = ($pageNo == 0)?0:(($pageNo-1)*10);
        if ($postType == 'all') {
            $posts = YcspPost::where('cause_id', $causeID)
                ->orderBy('id', 'DESC')
                ->limit($recPerPage)
                ->offset($offset)
                ->get();
            $totalRecords = YcspPost::where('cause_id', $causeID)
                ->count();
        } else {
            $posts = YcspPost::where('cause_id', $causeID)
                ->where('type', $postType)
                ->orderBy('id', 'DESC')
                ->limit($recPerPage)
                ->offset($offset)
                ->get();
            $totalRecords = YcspPost::where('cause_id', $causeID)
                ->where('type', $postType)
                ->count();
        }
        if ($totalRecords > 0) {
            foreach ($posts as $key => $post) {
                $discussionBoard[$key] = array();
                $discussionBoard[$key]['id'] = $post->id;
                $discussionBoard[$key]['cause_id'] = $post->cause_id;
                $discussionBoard[$key]['title'] = $post->title;
                $discussionBoard[$key]['content'] = $post->content;
                $discussionBoard[$key]['type'] = $post->type;
                $discussionBoard[$key]['date'] = date('Y-m-d H:i:s', strtotime($post->created_at));
                $discussionBoard[$key]['isCollapse'] = true;
                $user = $post->user;
                $totalComments = count($post->postComments);
                $discussionBoard[$key]['created_by'] = $user->firstname.' '.$user->lastname;
                if ($user->image) {
                    if (file_exists(public_path().'/uploads/profile/thumb/'.$user->image)) {
                        $discussionBoard[$key]['created_by_image'] = URL::to('/').'/public/uploads/profile/thumb/'.$user->image;
                    } else {
                        $user->image = '';
                    }
                }
                $discussionBoard[$key]['totalComments'] = $totalComments;
                $comments = $post->parentComments()->orderBy('id', 'DESC')->get();
                if (count($comments) > 0) {
                    $outerCommentArray = array();
                    foreach ($comments as $outerkey => $outerComment) {
                        $outerCommentArray[$outerkey] = array();
                        $outerCommentArray[$outerkey]['id'] = $outerComment->id;
                        $outerCommentArray[$outerkey]['question_id'] = $outerComment->question_id;
                        $outerCommentArray[$outerkey]['comment'] = $outerComment->comment;
                        $outerCommentArray[$outerkey]['date'] = date('Y-m-d H:i:s', strtotime($outerComment->created_at));
                        $commentUser = $outerComment->user;
                        $outerCommentArray[$outerkey]['submitted_by'] = $commentUser->firstname.' '.$commentUser->lastname;
                        if ($commentUser->image) {
                            if (file_exists(public_path().'/uploads/profile/thumb/'.$commentUser->image)) {
                                $outerCommentArray[$outerkey]['submitted_by_image'] = URL::to('/').'/public/uploads/profile/thumb/'.$commentUser->image;
                            } else {
                                $outerCommentArray[$outerkey]['submitted_by_image'] = '';
                            }
                        }
                        $outerCommentArray[$outerkey]['isCollapse'] = true;
                        $innerComments = $post->postComments()
                            ->where('parent', $outerComment->id)
                            ->orderBy('id', 'DESC')
                            ->get();
                        $outerCommentArray[$outerkey]['totalComments'] = count($innerComments);
                        if (count($innerComments) > 0) {
                            $innerCommentArray = array();
                            foreach ($innerComments as $innerKey => $comm) {
                                $innerCommentArray[$innerKey] = array();
                                $innerCommentUser = $comm->user;
                                $innerCommentArray[$innerKey]['id'] = $comm->id;
                                $innerCommentArray[$innerKey]['question_id'] = $comm->question_id;
                                $innerCommentArray[$innerKey]['cause_id'] = $post->cause_id;
                                $innerCommentArray[$innerKey]['comment'] = $comm->comment;
                                $innerCommentArray[$innerKey]['date'] = date('Y-m-d H:i:s', strtotime($comm->created_at));
                                $innerCommentArray[$innerKey]['submitted_by'] = $innerCommentUser->firstname.' '.$innerCommentUser->lastname;
                                if ($innerCommentUser->image) {
                                    if (file_exists(public_path().'/uploads/profile/thumb/'.$innerCommentUser->image)) {
                                        $innerCommentArray[$innerKey]['submitted_by_image'] = URL::to('/').'/public/uploads/profile/thumb/'.$innerCommentUser->image;
                                    } else {
                                        $innerCommentArray[$innerKey]['submitted_by_image'] = '';
                                    }
                                }
                            }
                            $outerCommentArray[$outerkey]['innerComments'] = $innerCommentArray;
                        }
                    }
                    $discussionBoard[$key]['outerComments'] = $outerCommentArray;
                }
            }
        }
        return ResponseClass::prepareResponse(
            array(
                'discussionBoard' => $discussionBoard,
                'totalItems' => $totalRecords,
                'postType' => $postType
            ),
            'success',
            ''
        );
    }

    /*
     * Request URL: http://domain/api/v1/download
     * Method: POST
     * Params: activityID, causeID
     * Return: success or error
     * Comment: Used to download a file from server
     */
    public function download(Request $request)
    {
        $data = Input::all();
        $first = (!isset($data));
        $second = (!isset($data['causeID']) || empty($data['causeID']));
        $third = (!isset($data['activityID']) || empty($data['activityID']));
        if ($first
            || $second
            || $third
        ) {
            return ResponseClass::prepareResponse(
                '',
                'error',
                Lang::get('message.noParameters')
            );
        }
        $causeID = Input::get('causeID');
        $activityID = Input::get('activityID');
        $activityData = Activity::where('cause_id', $causeID)
            ->where('id', $activityID)
            ->first();
        if (!$activityData) {
            return ResponseClass::prepareResponse(
                '',
                'error',
                'COMMON_ERROR_MESSAGE'
            );
        }
        $authUserInfo = Request::get('userInfo');
        $checkExist = UserActivity::where('user_id', $authUserInfo->id)
            ->where('cause_id', $causeID)
            ->where('activity_id', $activityID)
            ->first();
        if ($checkExist) {
            UserActivity::where('user_id', $authUserInfo->id)
                ->where('cause_id', $causeID)
                ->where('activity_id', $activityID)
                ->update(array('content' => $checkExist->content + 1));
        } else {
            $userActivity = array(
                'user_id' => $authUserInfo->id,
                'cause_id' => $causeID,
                'activity_id' => $activityID,
                'content' => 1
            );
            $status = UserActivity::create($userActivity);
            //student rewarding system
            $activityJsonData = json_decode($activityData->link_data);
            foreach ($activityJsonData as $jsonData) {
                $gamification = $jsonData->gamification;
            }
            $gamificationStatus = $activityData->gamification;
            if ($gamificationStatus == '1') {
                $pointData = YcspGamificationPoint::where('type', 'download_material_file')->first();
                $userRewarded = YcspStudentReward::where('gamification_point_id', $pointData->id)
                    ->where('cause_id', $causeID)
                    ->where('activity_id', $activityID)
                    ->where('student_id', $authUserInfo->id)
                    ->first();
                if (!$userRewarded) {
                    $rewardData = array(
                    'cause_id' => $causeID,
                    'activity_id' => $activityID,
                    'student_id' => $authUserInfo->id,
                    'gamification_point_id' => $pointData->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_activity_id' => $status->id
                );
                    YcspStudentReward::create($rewardData);
                }
            }
        }
        $linkData = json_decode($activityData->link_data);
        foreach ($linkData as $fileDetail) {
            $filename = $fileDetail->button_value;
        }
        $file = public_path().'/uploads/admin/'.$filename;
        if (file_exists($file)) {
            return response()->download($file);
        }
        return ResponseClass::prepareResponse(
            '',
            'error',
            ''
        );
    }