<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cause extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

    protected $table = 'ycsp_causes';

    protected $dates = array('deleted_at');
    
    protected $fillable = array(
        'title',
        'title_ar',
        'created_by',
        'created_at',
        'updated_at',
        'image'
    );

    // Cause has many mcqs
    public function mcqs()
    {
        return $this->hasMany('App\YcspMcqs', 'cause_id');
    }

    // Cause has many activities
    public function activity()
    {
        return $this->hasMany('App\Activity');
    }

    // Cause has many sections
    public function sections()
    {
        return $this->hasMany('App\YcspSection', 'cause_id');
    }

    // Cause has many rewards
    public function rewards()
    {
        return $this->hasMany('App\YcspStudentReward', 'cause_id');
    }

    // Cause has many user activity
    public function userActivity()
    {
        return $this->hasMany('App\UserActivity', 'cause_id');
    }

    // Cause has many active user
    public function userActive()
    {
        return $this->hasMany('App\YcspActiveUser', 'cause_id');
    }
}
